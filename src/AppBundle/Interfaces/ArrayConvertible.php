<?php


namespace AppBundle\Interfaces;

/**
 * Interface ArrayConvertible
 *
 * Array representation of entity
 *
 * @package AppBundle\Interfaces
 */
interface ArrayConvertible
{
    /**
     * Array representation of entity
     * @return array
     */
    public function toArray();
}