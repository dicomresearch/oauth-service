<?php

namespace AppBundle\Service;

use AppBundle\Entity\Template;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class TemplateFactory
 *
 * @package AppBundle\Service
 * @author Ruslan <r.sharafutdinov@dicoming.com>
 */
class TemplateFactory
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * TemplateFactory constructor.
     *
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param string $type
     * @return Template
     *
     * @todo вместо entity manager'а надо репозиторий инжектить и из него доставать
     */
    public function get($type)
    {
        return $this->om->getRepository('AppBundle:Template')->findOneBy(['type' => $type]);
    }
}
