<?php

namespace AppBundle\Entity;

/**
 * Class Template
 *
 * @package AppBundle\Entity
 * @author Ruslan <r.sharafutdinov@dicoming.com>
 */
class Template
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $templateText;

    /**
     * @var string
     */
    private $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Template
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTemplateText()
    {
        return $this->templateText;
    }

    /**
     * @param string $templateText
     * @return $this
     */
    public function setTemplateText($templateText)
    {
        $this->templateText = $templateText;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
