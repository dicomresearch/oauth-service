<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class Template
 *
 * @package AppBundle\Admin
 * @author Ruslan <r.sharafutdinov@dicoming.com>
 */
class TemplateAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', 'integer', ['disabled' => 'true'])
            ->add('name', 'text', ['disabled' => 'true'])
            ->add('templateText', 'textarea',
                [
                    'label' => $this->trans('Template text') . ' (html)',
                    'attr' => [
                        'rows' => 10,
                    ],
                    'required' => false,
                ]
            )
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', 'integer', ['label' => 'ID'])
            ->add('name', 'string', ['label' => $this->trans('Name')])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete');
    }
}
