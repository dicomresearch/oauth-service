<?php

namespace AppBundle\Block;

use AppBundle\Entity\Template;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class SupportInfo
 *
 * @package AppBundle\Block
 * @author Ruslan <r.sharafutdinov@dicoming.com>
 */
class SupportInfo extends BaseBlockService
{
    /**
     * @var Template
     */
    private $template;

    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'template' => 'AppBundle:block:support_info.html.twig',
        ]);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse(
            $blockContext->getTemplate(),
            [
                'block' => $blockContext->getBlock(),
                'content' => $this->getContent(),
            ],
            $response
        );
    }

    /**
     * @return string
     */
    private function getContent()
    {
        return $this->template !== null ? $this->template->getTemplateText() : '';
    }

    /**
     * @param Template $template
     * @return $this
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;

        return $this;
    }
}
