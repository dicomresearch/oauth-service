<?php
/**
 * Created by PhpStorm.
 * User: Damir Garifullin
 * Date: 22.10.2015
 * Time: 11:35
 */

namespace AppBundle\Composer;

use Composer\Script\CommandEvent;
use Symfony\Component\Filesystem\Filesystem;

class ScriptHandler extends \Sensio\Bundle\DistributionBundle\Composer\ScriptHandler
{
    protected static $options = array(
        'symfony-web-dir' => 'web',
    );

    /**
     * Копирует стилевики бутстрапа, необходимые для главного лейаута
     *
     * @param CommandEvent $event
     */
    public static function copyBootstrapFiles(CommandEvent $event)
    {
        $fs = new Filesystem();
        $options = static::getOptions($event);

        $webDir = $options['symfony-web-dir'];

        $fs->copy(
            __DIR__.'/../../../vendor/twbs/bootstrap/dist/css/bootstrap.css',
            $webDir.'/bootstrap/css/bootstrap.css',
            true
        );

        $fs->copy(
            __DIR__.'/../../../vendor/twbs/bootstrap/dist/css/bootstrap.min.css',
            $webDir.'/bootstrap/css/bootstrap.min.css',
            true
        );
    }

    /**
     * Накатывает миграции
     *
     * @param CommandEvent $event
     */
    public static function applyMigrateCommand(CommandEvent $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'update doctrine schema');

        if (null === $consoleDir) {
            return;
        }

        static::executeCommand(
            $event,
            $consoleDir,
            'doctrine:schema:update --force',
            $options['process-timeout']
        );
    }
}
