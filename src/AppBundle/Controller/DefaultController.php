<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OAuth2\ServerBundle\Entity\Client;
use OAuth2\ServerBundle\Manager\ClientManager;

class DefaultController extends Controller
{
    /**
     * View list of clients
     *
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        /**
         * @var ClientManager $clientManager
         */
        $clientManager = $this->get('oauth2.client_manager');
        $clients = $clientManager->find();

        $homepage = [];
        foreach ($clients as $client) {
            /**
             * @var Client $client
             */
            if ($client->getHomepage() !== '') {
                $homepage[] = $client->getHomepage();
            }
        }

        // админу добавляем доступ в админку
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $homepage[] = $this->generateUrl('sonata_admin_dashboard', [], true);
        }

        return $this->render('default/index.html.twig', ['homepage' => $homepage]);
    }
}
