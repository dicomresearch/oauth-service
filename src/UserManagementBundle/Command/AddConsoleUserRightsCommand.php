<?php

namespace UserManagementBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Exception\ShellCommandFailureException;
use Symfony\Component\Process\Process;
use Symfony\Component\Security\Acl\Exception\Exception;

/**
 * AddConsoleUserRights
 *
 * Команда фиксит проблему с правами на папки logs и cache после обновления композера на симфони проектах.
 * Берем текущего пользователя и ставим ему права такие же как и у web_user.
 */
class AddConsoleUserRightsCommand extends ContainerAwareCommand
{
    const COMMANDNAME = 'init:add-console-user-rights';

    protected function configure()
    {
        $this
            ->setName(AddConsoleUserRightsCommand::COMMANDNAME)
            ->setDescription('Create default user rights to app/cache & app/logs directories')
            ->addArgument(
                'server-user-name',
                InputArgument::OPTIONAL,
                'Web server user name (default: www-data)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $process = new Process('sudo apt-get install setfacl');
            $process->run(function ($type, $buffer) {
                if (Process::ERR === $type) {
                    throw new \Exception($buffer);
                }
            });
            $process->stop(3, SIGINT);
            $output->writeln('<fg=green>Successfuly installed setfacl</fg=green>');
        } catch (\Exception $e) {

            $output->writeln('<fg=red>Unable to install setfacl: ' . $e->getMessage() . ' </fg=red>');
            $output->writeln('Trying to set rights anyway ...');

            try {
                $defaultServerUserName = 'www-data';
                $serverUserName = $input->getArgument('server-user-name');
                if (!$serverUserName) {
                    $serverUserName = $defaultServerUserName;
                }
                $applicationPath = $this->getContainer()->getParameter('kernel.root_dir');

                $command = "setfacl -dR -m u:"
                    . $serverUserName
                    . ":rwX -m u:`whoami`:rwX "
                    . $applicationPath
                    . "/cache "
                    . $applicationPath
                    . "/logs";

                $process = new Process($command);
                $process->run(function ($type, $buffer) {
                    if (Process::ERR === $type) {
                        throw new \Exception($buffer);
                    }
                });

                $process->stop(3, SIGINT);
                $output->writeln('<fg=green>Successfuly set user rights</fg=green>');
            } catch (\Exception $e) {
                $output->writeln('<fg=red>Unable set user rights: ' . $e->getMessage() . ' </fg=red>');
            }
        }

        exit;
    }
} 