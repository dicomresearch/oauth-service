<?php


namespace UserManagementBundle\User;


use AppBundle\Interfaces\ArrayConvertible;
use OAuth2\ServerBundle\User\OAuth2UserInterface;

/**
 * Interface DicomUserInterface
 * @package UserManagementBundle\User
 */
interface DicomUserInterface extends \FOS\UserBundle\Model\UserInterface, OAuth2UserInterface, FormalUserInterface, ArrayConvertible
{

}