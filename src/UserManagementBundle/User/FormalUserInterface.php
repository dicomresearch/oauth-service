<?php


namespace UserManagementBundle\User;


/**
 * Interface FormalUserInterface
 *
 * User has a field:
 * firstName
 * secondName
 * middleName
 * birthDate
 *
 * @package UserManagementBundle\User
 */
interface FormalUserInterface
{

    /**
     * Set FirstName
     *
     * @param  string $firstName
     * @return $this
     */
    public function setFirstName($firstName);

    /**
     * Get FirstName
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Set LastName
     *
     * @param  string $lastName
     * @return $this
     */
    public function setLastName($lastName);

    /**
     * Get LastName
     *
     * @return string
     */
    public function getLastName();

    /**
     * Get MiddleName
     *
     * @return string
     */
    public function getMiddleName();

    /**
     * Set MiddleName
     *
     * @param  string $middleName
     * @return $this
     */
    public function setMiddleName($middleName);

    /**
     * Set BirthDate
     *
     * @param  \DateTime $birthDate
     * @return $this
     */
    public function setBirthDate($birthDate);

    /**
     * Get BirthDate
     *
     * @return string
     */
    public function getBirthDate();
}