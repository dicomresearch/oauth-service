<?php
/**
 * Created by PhpStorm.
 * User: alsu
 * Date: 01.04.15
 * Time: 12:33
 */

namespace bundles\tests\Users;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetUserInfoTest extends WebTestCase{

    public function testGetUserInfo()
    {
        $client = static::createClient();
//        $client->restart();
        $client->request('GET', '/userManagement/api/get?id=1 ',
            array(),
            array(),
            array(
                'HTTP_HOST'       => 'oauth.local',
//                'HTTP_USER_AGENT' => 'Firefox/36.0',
            ));
        $crawler = $client->getResponse()->getContent();
//        print_r($crawler);
        $this->assertContains(
            '{"user":{"id":1,"username":"admin","roles":["ROLE_SUPER_ADMIN","ROLE_USER"],"scope":"","firstname":"\u041f\u043e \u0432\u0441\u0435\u043c","lastname":"\u0413\u043b\u0430\u0432\u043d\u044b\u0439","middlename":"\u0413\u043b\u0430\u0432\u043d\u044b\u043c","email":"admin@helterclinic.ru","birthdate":{"date":"2015-06-11 00:00:00","timezone_type":3,"timezone":"Europe\/Moscow"}}}',
            $client->getResponse()->getContent()
        );
    }

} 