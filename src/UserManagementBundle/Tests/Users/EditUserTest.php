<?php
/**
 * Created by PhpStorm.
 * User: alsu
 * Date: 01.04.15
 * Time: 12:57
 */

namespace bundles\tests\Users;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditUserTest extends WebTestCase{

    public function testEditUser()
    {
        $client = static::createClient();
        $client->restart();
        $data = [
            'id' => 14,
            'username' => 'petya',
            'password' => 'fsdfsdfsdf'
        ];
        $client->request('POST', 'userManagement/api/update',
//            array('data' =>'{"id": 14, "username":"petya", "password":"kyrlishkin"}'),
            ['data' => json_encode($data)],
            array(),
            array(
                'HTTP_HOST'       => 'oauth.local',
//                'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            ));
//        $crawler = $client->getResponse()->getContent();
//        print_r($crawler);
        $this->assertContains(
            '{"update":{"id":14,"username":"petya","roles":[],"scope":""}}',
            $client->getResponse()->getContent()
        );
    }

} 