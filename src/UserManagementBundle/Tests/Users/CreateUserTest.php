<?php
/**
 * Created by PhpStorm.
 * User: alsu
 * Date: 01.04.15
 * Time: 12:31
 */

namespace bundles\tests\Users;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class CreateUserTest extends WebTestCase{

    public function testCreateUser()
    {
        $client = static::createClient();
        $client->restart();
        $data = ['username'=>'ivan9',
            'password'=>'kyrlishkin'];
        $client->request('POST', 'userManagement/api/create',
            array('data' => json_encode($data)),
            array(),
            array(
                'HTTP_HOST'       => 'oauth.local',
            ));
        $content = $client->getResponse()->getContent();
        $status = $client->getResponse()->getStatusCode();
//        $id = $content["id"];
//        $request = $this->req;
//        $id= $request->request->get('id');
//        print_r($id);
//        $this->assertContains('{"user":{"id":24,"username":"ivan6","roles":[],"scope":""}}',
//            $content);
    }


    public function testDeleteUser()
    {
        $client=static::createClient();
        $client->restart();
        $client->request('GET', 'userManagement/api/delete?id=13',
            array(),
            array(),
            array(
                'HTTP_HOST'       => 'oauth.local',
            ));
        $crawler = $client->getResponse()->getContent();
        $this->assertContains(
            '{"delete":true}',
            $client->getResponse()->getContent()
        );
    }

} 