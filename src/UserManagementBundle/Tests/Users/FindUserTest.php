<?php
/**
 * Created by PhpStorm.
 * User: alsu
 * Date: 03.04.15
 * Time: 13:04
 */

namespace bundles\tests\Users;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FindUserTest extends WebTestCase{

    public function testFindUser()
    {
        $client = static::createClient();
        $client->restart();
        $data = [
            'id' => 1
        ];
        $client->request('POST', 'userManagement/api/find',
//            array('data' =>'{"id": 14, "username":"petya", "password":"kyrlishkin"}'),
            ['criteria' => json_encode($data)],
            array(),
            array(
                'HTTP_HOST'       => 'oauth.local',
//                'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            ));
//        $crawler = $client->getResponse()->getContent();
//        print_r($crawler);
        $this->assertContains(
            '{"users":[{"id":14,"username":"petya","roles":[],"scope":""}]}',
            $client->getResponse()->getContent()
        );
    }

} 