<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 08.05.15
 * Time: 10:04
 */

namespace UserManagementBundle\Components;

use dicom\kendoUiQueryBuilder\EntityManagerLink;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use dicom\kendoUiQueryBuilder\KendoUIQueryBuilder;

class DoctrineQueryBuilderConstruct
{
    /**
     * @var \Doctrine\ORM\Mapping\ClassMetadata
     */
    private $classMetadata;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var EntityManager
     */
    private $entityManager;


    public function __construct($entityName, EntityManager $entityManager)
    {
        $this->entityName = $entityName;
        $this->entityManager = $entityManager;
        $this->classMetadata = $this->entityManager->getClassMetadata($entityName);

        //todo вынести в DI
        EntityManagerLink::setEm($entityManager);
    }

    /**
     * @param EntityRepository $entityRepository
     * @param $criteria
     * @param $orderBy
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilder(EntityRepository $entityRepository,  $criteria, $orderBy)
    {
        $queryBuilder = $entityRepository->createQueryBuilder($this->getTableName());

        $kendoQueryBuilder = new KendoUIQueryBuilder($this->entityName, $this->classMetadata);
        $queryBuilder = $kendoQueryBuilder->createQueryBuilder($queryBuilder, $criteria, $orderBy);

        return $queryBuilder;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->classMetadata->getTableName();
    }


}