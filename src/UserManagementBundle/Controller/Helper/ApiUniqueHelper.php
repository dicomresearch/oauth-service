<?php

namespace UserManagementBundle\Controller\Helper;
use OAuth2\ServerBundle\Entity\User;
use UserManagementBundle\Manager\UserManager;

/**
 * ApiUniqueHelper 
 */
class ApiUniqueHelper 
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * init
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Check if this email is already taken
     *
     * @param User $user
     * @return bool
     */
    public function checkEmailIsAlreadyTaken(\OAuth2\ServerBundle\Entity\User $user)
    {
        $searchUser =  $this->userManager->findUserByEmail($user->getEmail());
        return !is_null($searchUser);
    }

    /**
     * @param User $user
     */
    public function checkUsernameIsAlreadyTaken(\OAuth2\ServerBundle\Entity\User $user)
    {
        $searchUser =  $this->userManager->findUserByUsername($user->getUsername());
        return !is_null($searchUser);
    }
} 