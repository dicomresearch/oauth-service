<?php


namespace UserManagementBundle\Controller\Helper;


use UserManagementBundle\User\DicomUserInterface;

/**
 * Class ApiHelper
 *
 * Hepler для нпреобразования данных, полученных с API
 *
 * @package UserManagementBundle\Controller\Helper
 */
class ApiDataFetchHelper
{
    /**
     * @param DicomUserInterface $user
     * @param $dataFromApi
     * @return DicomUserInterface
     */
    public function setUserData(DicomUserInterface $user, $dataFromApi)
    {
        if (!empty($dataFromApi['username'])) {
            $user->setUsername($dataFromApi['username']);
        }

        if (!empty($dataFromApi['password'])) {
            $user->setPlainPassword($dataFromApi['password']);
        }

        if (!empty($dataFromApi['email'])) {
            $user->setEmail($dataFromApi['email']);
        }

        if (!empty($dataFromApi['roles'])) {
            $user->setRoles($dataFromApi['roles']);
        }

        if (!empty($dataFromApi['scopes'])) {
            $user->setScopes($dataFromApi['scopes']);
        }

        if (!empty($dataFromApi['firstname'])) {
            $user->setFirstName($dataFromApi['firstname']);
        }

        if (!empty($dataFromApi['lastname'])) {
            $user->setLastName($dataFromApi['lastname']);
        }

        if (!empty($dataFromApi['middlename'])) {
            $user->setMiddleName($dataFromApi['middlename']);
        }

        if (!empty($dataFromApi['birthdate'])) {
            $user->setBirthDate($dataFromApi['birthdate']);
        }

        return $user;
    }
}