<?php

namespace UserManagementBundle\Controller;
use Doctrine\Common\Collections\Criteria;
use FOS\UserBundle\Model\UserManagerInterface;
use OAuth2\ServerBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserManagementBundle\Components\DoctrineQueryBuilderConstruct;
use UserManagementBundle\Exception\UserManagementException;
use UserManagementBundle\Manager\UserManager;

/**
 * ApiController
 * Апи контроллер для работы с пользователями
 */
class ApiController extends Controller
{
    /**
     * Создание пользователя
     */
    public function createAction(Request $request)
    {
        $data = json_decode($request->get('data'), true);
        $user = $this->getUserManager()->createUser();
        $user = $this->container->get('user_management.api.data_fetch_helper')->setUserData($user, $data);

        if ($this->container->get('user_management.api.unique_helper')->checkEmailIsAlreadyTaken($user)) {
            return new JsonResponse([
                'error' => $this->get('translator')
                    ->trans("User with email: %email% already exists", [
                        '%email%' => $user->getEmail()
                    ])
            ]);
        }

        if ($this->container->get('user_management.api.unique_helper')->checkUsernameIsAlreadyTaken($user)) {
            return new JsonResponse([
                'error' => $this->get('translator')
                    ->trans("User with username: %userName% already exists", [
                        '%userName%' => $user->getUsername()
                    ])
            ]);
        }

        // активируем пользователя
        $user->setEnabled(true);
        $this->getUserManager()->updateUser($user);

        return new JsonResponse(['user' => $user->toArray()]);
    }

    /**
     * Обновление пользователя
     */
    public function updateAction(Request $request)
    {
        $data = json_decode($request->get('data'), true);
        $user = $this->getUserManager()->findUserById($data['id']);
        $user = $this->container->get('user_management.api.data_fetch_helper')->setUserData($user, $data);
        $this->getUserManager()->updateUser($user);
        return new JsonResponse(['update' => $user->toArray()]);
    }

    /**
     * Получить пользователя по id
     */
    public function getAction(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getUserManager()->findUserById($id);
        if (null === $user) {
            throw $this->createNotFoundException(sprintf('User with id %s not found', $id));
        }
        return new JsonResponse(['user' => $user->toArray()]);
    }

    /**
     * Найти пользователей
     *
     * @param array criteria array('name' => 'foo')
     * @param array orderBy array('price' => 'ASC')
     * @param integer limit
     * @param integer offset
     */
    public function findAction(Request $request)
    {
        $criteria = json_decode($request->get('criteria'), true);
        $orderBy = json_decode($request->get('orderBy', '[]'), true);
        $limit = $request->get('limit');
        $offset = $request->get('offset');

        $entityName = $this->getUserManager()->getClass();
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $userEntityRepository = $entityManager->getRepository($entityName);

        $queryBuilder = new DoctrineQueryBuilderConstruct($entityName, $entityManager);
        $userEntityQuery = $queryBuilder->createQueryBuilder($userEntityRepository, $criteria, $orderBy);

        // общее число пользователей
        $totalUsers = count($userEntityQuery->getQuery()->getScalarResult());

        // пользователи с учетом пагинации
        $users = $userEntityQuery
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();

        $usersInfo = [];
        foreach ($users as $user) {
            $usersInfo[] = $user->toArray();
        }
        return new JsonResponse(['users' => $usersInfo, 'total' => $totalUsers]);
    }

    /**
     * Удалить пользователя
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getUserManager()->findUserById($id);
        if (null === $user) {
            return new JsonResponse(['error' => $this->get('translator')->trans('User with id %id% not found', ['%id%' => $id])]);
        }
        $this->getUserManager()->deleteUser($user);
        return new JsonResponse(['delete' => true]);
    }

    /**
     * @return UserManager
     */
    private function getUserManager()
    {
        return $this->get('user_management.user_manager');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function changePassAction(Request $request)
    {
        $data = json_decode($request->get('data'), true);

        if (!array_key_exists('oldPassword', $data)) {
            return new JsonResponse(['error' => $this->get('translator')->trans('Old password error')]);
        }
        $oldPassword = $data['oldPassword'];

        if (!array_key_exists('newPassword', $data)) {
            return new JsonResponse(['error' => $this->get('translator')->trans('New password error')]);
        }
        $newPassword = $data['newPassword'];

        if (!array_key_exists('userId', $data)) {
            return new JsonResponse(['error' => $this->get('translator')->trans('User id error')]);
        }
        $userId = $data['userId'];

        $user = $this->getUserManager()->findUserById($userId);

        if (!$this->getUserManager()->verifyPassword($user, $oldPassword)) {
            return new JsonResponse(['error' => $this->get('translator')->trans('The old password is incorrect')]);
        }

        // установим новый пароль
        $user->setPlainPassword($newPassword);
        $this->getUserManager()->changePassword($user);

        return new JsonResponse();
    }
}