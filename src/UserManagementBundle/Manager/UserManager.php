<?php

namespace UserManagementBundle\Manager;

use OAuth2\ServerBundle\Entity\User;

class UserManager extends \FOS\UserBundle\Doctrine\UserManager
{
    /**
     * Find User By id
     *
     * @param int $id
     * @return \FOS\UserBundle\Model\UserInterface|object
     */
    public function findUserById($id)
    {
        return $this->findUserBy(array('id' => $this->canonicalizeEmail($id)));
    }

    /**
     * Find users by criteria
     *
     * @param array $criteria
     * @param array $orderBy
     * @param null|int $limit
     * @param null|int $offset
     * @return User
     */
    public function findUsersBy(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param User $user
     * @param $oldPassword
     * @return boolean
     */
    public function verifyPassword(User $user, $oldPassword)
    {
        return $this->getEncoder($user)->isPasswordValid($user->getPassword(), $oldPassword, $user->getSalt());
    }

    /**
     * @param User $user
     */
    public function changePassword(User $user)
    {
        $this->updatePassword($user);
        $this->objectManager->flush();
    }
}