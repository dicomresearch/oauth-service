<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 18.03.15
 * Time: 11:53
 */

namespace UserManagementBundle\Admin;

use FOS\UserBundle\Model\UserManagerInterface;
use OAuth2\ServerBundle\Entity\User;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends Admin
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * установка сортировки по умолчанию
     * @var array
     */
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by'    => 'id'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', 'integer', ['disabled' => 'true'])
            ->add('username')
            ->add('plainPassword', 'text', ['label' => $this->trans('password'), 'required' => $this->getSubject()->isNew() ? true : false])
            ->add('lastname', null, ['label' => $this->trans('Last name')])
            ->add('firstname', null, ['label' => $this->trans('First name')])
            ->add('middlename', null, ['label' => $this->trans('Middle name')])
            ->add('email', null, ['label' => $this->trans('Email')])
            ->add('birthdate', 'sonata_type_date_picker', [
                'label' => $this->trans('Birth date'),
                'dp_language' => 'ru',
                'format' => 'd.M.yyyy',
                'required' => false
            ])
            ->add('locked', null, ['label' => $this->trans('Locked') . ' ( блокировка пользователя )', 'required' => false])
            ->add('enabled', null, ['label' => $this->trans('Enabled') . ' ( активация пользователя )', 'required' => false])
            ->add('roles', 'choice', [
                'choices' => $this->getUserRoles(),
                'multiple' => true,
                'required' => false
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('birthdate')
 			->add('roles', null, ['label' => 'Roles'])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', 'integer', ['label' => $this->trans('ID')])
            ->add('FIO', 'string', ['label' => $this->trans('FIO'), 'sortable' => 'lastname'])
            ->add('username', ' string', ['label' => $this->trans('Username')])
            ->add('email', 'string', ['label' => $this->trans('Email')])
            ->add('birthdate', 'date', [
                'label' => $this->trans('Birth date'),
                'format' => 'd.m.Y'
            ])
            ->add('rolesAsString', 'string', [
                'label' => 'Roles',
                'sortable' => 'roles',
            ])
            ->add('enabled', null, ['label' => $this->trans('Enabled') . ' ( активирован )'])
            ->add('locked', null, ['label' => $this->trans('Locked') . ' ( заблокирован )'])
        ;
    }

    /**
     * @param User $object
     */
    public function preUpdate($object)
    {
        if ($object->getPlainPassword()) {
            $this->getUserManager()->updatePassword($object);
        }

        parent::preUpdate($object);
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager($userManager)
    {
        $this->userManager = $userManager;
    }


    /**
     * получение списка ролей, которые мы можем назначить
     * другому пользователю при создании\редактировании
     * возвращает массив вида ['ROLE_ID' => 'ROLE_NAME']
     *
     * @return array
     */
    public function getUserRoles()
    {
        // список всех ролей в системе
        $systemRoles = $this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles');

        // роли зарегистрированного пользователя
        $userRoles = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getRoles();

        $result = [];
        foreach ($userRoles as $userRole) {
            // по умолчанию можем наделять новых\редактируемых пользователей своей ролью
            $role = $userRole->getRole();
            $result[$role] = $role;

            // добавим роли из подролей
            $result = array_merge($result, $this->getSubRoles($systemRoles, $role));
        }

        $result = array_unique($result);

        return $result;
    }

    /**
     * пробегая по подролям, мы проверяем, является ли подроль
     * составной, т.е. является ли она сама одной из основных ролей
     * возвращает массив вида ['ROLE_ID' => 'ROLE_NAME']
     *
     * @param array $allRoles
     * @param $role
     * @return array
     */
    public function getSubRoles(array $allRoles, $role)
    {
        if (!array_key_exists($role, $allRoles)) {
            return [];
        }

        $result = [];
        foreach($allRoles[$role] as $subRole) {
            // проверка, что подроль является основной ролью
            if (array_key_exists($subRole, $allRoles)) {
                $result[$subRole] = $subRole;
            }
        }

        return $result;
    }
}