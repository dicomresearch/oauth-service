<?php

namespace AuthenticationBundle\Handler;

/**
 * CookieClearingHandler
 *
 * Чистит куки
 */
class CookieClearingHandler 
{
    /**
     * @var array
     */
    private $cookies;

    /**
     * @param $cookies
     */
    public function __construct($cookies)
    {
        $this->cookies = $cookies;
    }

    public function clear($response)
    {
        foreach ($this->cookies as $cookieName => $cookieData) {
            $response->headers->clearCookie($cookieName);
        }
        return $response;
    }
} 