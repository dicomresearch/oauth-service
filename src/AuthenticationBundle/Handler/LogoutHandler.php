<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 06.04.15
 * Time: 15:33
 */

namespace AuthenticationBundle\Handler;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\CookieClearingLogoutHandler;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutHandler implements LogoutHandlerInterface, LogoutSuccessHandlerInterface
{
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function logout(Request $request, Response $response, TokenInterface $token)
    {

    }

    public function onLogoutSuccess(Request $request)
    {
        if (!$referer = $request->query->get('redirect_url')) {
            if (!$referer = $request->headers->get('referer')) {
                $referer = $this->router->generate('homepage');
            }
        }

        // TODO: task952: к сожалению добавление в конфиг сервиса в logout.handlers не срабатывает, поэтому пока так
        // TODO: в будущем сделать чтобы CookieClearingHandler прописывался в конфиге и отрабатывал автоматом
        $response =  new RedirectResponse($referer);
        $cookieHandler = new CookieClearingHandler($request->cookies->all());
        $response = $cookieHandler->clear($response);

        return $response;
    }
}