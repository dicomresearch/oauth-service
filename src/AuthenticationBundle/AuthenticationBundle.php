<?php

namespace AuthenticationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AuthenticationBundle
 *
 * Бандл предназначен для аутентификации пользователей
 *
 * @package AuthenticationBundle
 */
class AuthenticationBundle extends Bundle
{
}
