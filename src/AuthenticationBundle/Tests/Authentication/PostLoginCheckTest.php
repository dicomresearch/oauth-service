<?php
/**
 * Created by PhpStorm.
 * User: alsu
 * Date: 20.03.15
 * Time: 15:03
 */

namespace bundles\tests\Authentication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class PostLoginCheckTest extends WebTestCase{

    public function testPostLoginCheck()
    {
        $client = static::createClient();
        $client->followRedirects();
        $request = $client->request('POST', '/login_check',
            array(),
            array(),
            array('HTTP_HOST'       => 'oauth.local',
                'PHP_AUTH_USER' => 'admin', 'PHP_AUTH_PW' => '123456',
            ));
        $this->assertGreaterThan(0, $request->filter('html:contains("Welcome!")')->count());
    }

//    public function testTryGetAuthenticationKeyAndVerify()
//    {
//        //получить из конфига тестовые данные. Сейчас я их заполню руками
//        $clientApplicationName = 'teleradiology';
//        $clientApplicationSecret = 'fsdffsdfdjsflhdskhfkds';
//        $clientApplicationRedirectUrl = 'http://sdld.local/teleradiology/admin/backend/user/authenticate';
//
//        $username = 'admin';
//        $password = '123456';
//        //конец конфига
//
//        $client = new OauthClientApplication($clientApplicationName , $clientApplicationSecret, $clientApplicationRedirectUrl);
//        //or
//        $client = OauthClientApplication::createByConfig($config);
//
//        $authenticationManager = new TestAuthenticationManager($client);
//
//        $authCode = $authenticationManager->getAuthCodeforUser($username, $password);
//        $this->assertNotEmpty($authCode);
//
//        $accessKey = $authenticationManager->getAccessKeyByAuthenticationCode($authCode);
//        //$this->assert...($accessKey);
//
//        $verifyResult = $authenticationManager->verifyAccessKey($accessKey);
//        $this->assert..($verifyResult);
//
//    }
//
//
//    public function testGetAuthenticationKeyAndVerify()
//    {
//        $client = static::createClient();
//        $client->followRedirects(false);
//        //todo $authenticationCode = getAuthenticationCode($client)
//        $crawler = $client->request('GET', '/authorize?client_id=teleradiology&response_type=code&scope=defaultScope&state=defaultState&redirect_uri=http://sdld.local/teleradiology/admin/backend/user/authenticate',
//            array(),
//            array(),
//            array('HTTP_HOST' => 'oauth.local',
//                'PHP_AUTH_USER' => 'admin',
//                'PHP_AUTH_PW'   => '123456',
////                'HTTP_CONTENT' => 'text/html; charset=utf-8'
//            ));
//        $uri = $client->getResponse()->headers->get('location');
//        $code=null;
//        $parseUrl = parse_url($uri);
//        $query = parse_str($parseUrl['query'], $result);
//        $code = $result['code'];
//
//        //todo getAuthKeyBuAuthenticationCode($client, $code)
//        $this->assertNotEmpty($code);
//        $data = ['access_token'=>$code];
//        $client->request('POST', '/token',
//            array('grant_type'=>'authorization_code',
//                'client_id'=>'teleradiology',
//                'client_secret'=>'q2qjbaxfsao0kw4ccco0sog8sckcgcg',
//                'code'=>$code,
//                'redirect_uri'=>'http%3A%2F%2Fsdld.local%2Fteleradiology%2Fadmin%2Fbackend%2Fuser%2Fauthenticate'),
//            array(),
//            array(
//                'HTTP_HOST' => 'oauth.local'
//            ));
//        $crawler = $client->getResponse()->getContent();
//        $crawler = json_decode($crawler);
//        $access_token = $crawler->access_token;
//        $client->request('POST', '/verify',
//            array('access_token'=>$access_token),
//            array(),
//            array(
//                'HTTP_HOST' => 'oauth.local'
//            ));
//        $crawler = $client->getResponse()->getContent();
//        print_r($crawler);
//    }

} 