<?php

namespace AuthenticationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AuthenticationBundle:Default:index.html.twig', array('name' => $name));
    }

    public function userInfoAction()
    {
        var_dump($this->getUser());
        return new Response();
    }

}
