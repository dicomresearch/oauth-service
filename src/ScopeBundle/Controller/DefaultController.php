<?php

namespace ScopeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ScopeBundle:Default:index.html.twig', array('name' => $name));
    }
}
