<?php

namespace ScopeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ScopeBundle
 *
 * Scope bundle предназначен для управления Scope'ами
 *
 * What is scope?
 * The use of Scope in an OAuth2 application is often key to proper permissioning.
 * Scope is used to limit the authorization granted to the client by the resource owner.
 * The most popular use of this is Facebook’s ability for users to authorize a variety of different functions
 * to the client (“access basic information”, “post on wall”, etc).
 *
 * @package ScopeBundle
 */
class ScopeBundle extends Bundle
{
}
