<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 24.03.15
 * Time: 13:34
 */

namespace ScopeBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class ScopeAdmin extends Admin
{
    // установка сортировки по умолчанию
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by'    => 'scope'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', 'integer', ['disabled' => 'true'])
            ->add('scope')
            ->add('description')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('scope')
            ->add('description')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('scope', null, ['label' => 'Scope'])
            ->add('description', null, ['label' => 'Description'])
        ;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $other = $this->modelManager->findOneBy($this->getClass(), ['scope' => $object->getScope()]);

        if (null !== $other && $other->getId() !==$object->getId()) {
            $errorElement
                ->with('scope')
                ->addViolation($this->trans('The unique field must be unique!'))
                ->end();
        }
    }
}