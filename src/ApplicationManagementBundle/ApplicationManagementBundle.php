<?php

namespace ApplicationManagementBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ApplicationManagementBundle
 *
 * Бандл предназначен для управления (клиентскими) приложениями, которые пользуются OauthService'ом
 *
 * @package ApplicationManagementBundle
 */
class ApplicationManagementBundle extends Bundle
{
}
