<?php


namespace ApplicationManagementBundle\Admin;


use OAuth2\ServerBundle\Entity\Client;
use OAuth2\ServerBundle\Manager\ClientManager;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class ApplicationAdmin
 *
 * Предназначен для показа админки управления приложениями, сгенерированной Sonata admin
 *
 * @package ApplicationManagementBundle\Admin
 */
class ApplicationAdmin extends Admin
{
    /**
     * @var ClientManager
     */
    private $oauthClientManager;

    // установка сортировки по умолчанию
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by'    => 'client_id'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('client_id', 'text')
            ->add('client_secret', 'text', ['read_only' => true, 'required' => false])
            ->add('redirect_uri', 'collection', ['allow_add' => true, 'allow_delete' => true, 'delete_empty' => true, 'type' => 'url'])
            ->add('grant_types', 'collection', ['allow_add' => true, 'allow_delete' => true, 'delete_empty' => true, 'type' => 'text'])
            ->add('scopes', 'collection', ['allow_add' => true, 'allow_delete' => true, 'delete_empty' => true, 'type' => 'text'])
            ->add('homepage', 'url')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('client_id')
            ->add('homepage')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('client_id')
            ->add('client_secret')
            ->add('homepage')
        ;
    }

    /**
     * @return ClientManager
     */
    public function getOauthClientManager()
    {
        return $this->oauthClientManager;
    }

    /**
     * @param ClientManager $oauthClientManager
     */
    public function setOauthClientManager($oauthClientManager)
    {
        $this->oauthClientManager = $oauthClientManager;
    }

    /**
     * Генерируем пароль приложения автоматически
     *
     * @param Client $object
     * @return mixed|void
     */
    public function prePersist($object)
    {
        /*todo. по хорошему, нужно не просто генерировать пароль средствами oauth.clientManager
        а делать создание объекта им. Для этого нужно переопределить ModelManager, в котором импользовать oauth.clientManager */
        $secret = $this->getOauthClientManager()->generateSecret();
        $object->setClientSecret($secret);
        parent::prePersist($object);
    }


}